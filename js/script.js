'use strict';

const tabTitles = document.querySelector('.tabs');
const tabContents = document.querySelectorAll('.tabs-content li');
const names = document.querySelectorAll('.tabs-title');


tabTitles.addEventListener('click', function(event) {
    names.forEach((item) => {
        if(item.innerHTML === event.target.innerHTML){
            item.classList.add('active');                     
        } else {
            item.classList.remove('active');            
        }         
    }); 
    
        tabContents.forEach((item) =>{
        if(item.getAttribute('data-content') === event.target.innerHTML) {
            item.classList.add('active-content');  
        } else {
            item.classList.remove('active-content');
        }
    });
});



